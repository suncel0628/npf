<?php

namespace Npf\Exception {

    use Npf\Core\Exception;

    /**
     * Class InvalidOperate
     * @package Exception
     */
    class InvalidOperate extends Exception
    {
        protected $error = 'invalid_operate';
    }
}
